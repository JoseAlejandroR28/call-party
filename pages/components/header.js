import  React, { Component } from 'react'  
import socketIOClient from 'socket.io-client'
var socket = socketIOClient('/')

class Header extends Component {

    constructor(){
        super()

        this.state = {
            isCalling: false,
        }
        socket.on('test',data => console.log(data) )
    }

    handlerClick = () => {

    }
    
    render(){
        return <div style={barStyle}>
            <h1 style={textTitle}>Call Party</h1>
            <h3 style={textSubtitle}>Just a simple demo</h3>

        </div>
    }
}

const barStyle = {
    background:'#ccc',
    padding:'10px',
    paddingLeft:'20px',
    borderBottom: '1px solid #fff'
} 

const textTitle = {
    margin:'0px',
}

const textSubtitle = {
    margin: '0px',
    marginTop: '10px',
}

const btnCamStyle = {
    height: '60px',
    position: 'absolute',
    right: '20px',
    top: '16px',
}

export { Header, socket } 