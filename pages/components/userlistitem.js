import  React, { Component } from 'react'  

class UserListItem extends Component {

    constructor(props) {
        super(props)
        this.state = {
           
        }   
    }

    componentDidMount(){

    }

    setSourceURI(URI){
        this.setState({
            sourceURI: URI
        })
        
        navigator.getUserMedia({video: true, audio: true}, function (stream) {
            this.videoRef.current.srcObject = stream;
            pc.addStream(stream);
        }, error)
    }

    componentDidUpdate() {
        this.updateVideoStream()
      }

    handlerClick = (user) => {
        this.props.handlerClick(this.props.user)
    }

    render(){
        return <div onClick={this.handlerClick} style={userListItemStyle}>
                <span style={{}}>{this.props.user.name}</span>
        </div>
    }
}

const userListItemStyle = {
    background: '#666',
    border: '1px solid #888',
    padding: '10px',
}

export default UserListItem