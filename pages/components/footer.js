import  React, { Component } from 'react'  

class Footer extends Component {

    constructor(){
        super()
    }

    render(){
        return <div style={barStyle}>
            developed by <a style={{color:'green'}} href="https://www.linkedin.com/in/jose-alejandro-realza-rojas-3aa08452/" title="Go to my profile" target="_blank">Jose Alejandro Realza</a> - source: <a style={{color:'blue'}} href="https://gitlab.com/JoseAlejandroR28/call-party" target="_blank">https://gitlab.com/JoseAlejandroR28/call-party</a>
        </div>
    }
}

const barStyle = {
    background:'#222',
    color: '#ccc',
    padding:'10px',
    paddingLeft:'20px',
    textAlign: 'center',
} 

const textTitle = {
    margin:'0px',
}

const textSubtitle = {
    margin: '0px',
    marginTop: '10px',
}

const btnCamStyle = {
    height: '60px',
    position: 'absolute',
    right: '20px',
    top: '16px',
}

export default Footer