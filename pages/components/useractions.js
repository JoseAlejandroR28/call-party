import  React, { Component } from 'react'  
import {socket} from './header' 

class UserActions extends Component {

    constructor() {
        super()
        this.state = {
            name: null,
            isCalling: false,
        }   
    }

    buttonCall(){
        return {
            background: !this.props.isCallActive ? '#668804': 'red',
            border: !this.props.isCallActive ? '1px solid rgb(130 202 117)':  '1px solid rgb(255 95 95)',
            borderRadius: 50,
            color: '#fff',
            fontWeight: 'bold',
            margin: '10px auto',
            padding: '20px',
        }
    }

    render(){

        return <ul style={userActionsStyle}>

                <span onClick={this.props.handlerClick} style={this.buttonCall()}> { !this.props.isCallActive ? 'Call': 'X' } </span>
        </ul>
    }
}

const userActionsStyle = {
    background: '#111',
    listStyle: 'none',
    margin: '0',
    padding: 0,
    with: '100px',
    display: 'flex',
    justifyContent: 'center',
    textTransform: 'uppercase',
}

export default UserActions