import  React, { Component } from 'react'

class UserDisplay extends Component {

    constructor(props) {
        super(props)
        this.state = {
            user: props.user || null,
            sourceURI: null,
            isCallActive: false,
        }   
        this.videoRef = React.createRef()
        this.state.user = props.user
        console.log(props)
    }

    componentDidMount(){

    }

    setSourceURI(URI){
        this.setState({
            sourceURI: URI
        })
        
        navigator.getUserMedia({video: true, audio: true}, function (stream) {
            this.videoRef.current.srcObject = stream;
            pc.addStream(stream);
        }, error)
    }

    componentDidUpdate() {
        if (this.props.stream != null ) {
            this.updateVideoStream()
        }
      }

    updateVideoStream = () =>  {
        if (this.videoRef.current != null && this.videoRef.current.srcObject !== this.props.stream) {
          this.videoRef.current.srcObject = this.props.stream
          
          document.getElementById(this.props.user.id).play();
        }
    }

    render(){
        return <div style={displayStyle}>
                {
                    this.props.stream == null ? (
                        <img src={'./images/user-profile-icon.png'} style={userPicture} />
                        ): (
                            this.props.muted ? (
                                <video id={this.props.user.id} ref={this.videoRef} style={videoStyle} muted />
                            ) : (
                                <video id={this.props.user.id} ref={this.videoRef} style={videoStyle} muted  />
                            ) 
                            
                            )
                        }
                {
                    this.props.captions != null && this.props.captions.length > 0? (
                        <div style={captionBoxStyle}>
                            <div style={captionText}>{this.props.captions}</div>
                        </div>
                    ): (
                       ''
                    )
                }
                
                <span style={textUserName}>{this.props.user.name}</span>
        </div>
    }
}

const displayStyle = {
    position: 'relative',
    alignItems:'stretch',
    display:'inline-block',
   // height: '200px',
    width: '100%',
}

const userPicture = {
    height: '100px',
    display: 'block',
    margin: '0 auto',
}

const videoStyle = {
    position: 'relative',
    //height: '100%',
    width: '100%',
    top: 0,
    left: 0,
}

const textUserName = {
    color: '#fff',
    display:'block',
    fontSize: '18px',
    fontWeight: 'normal',
    left: '10px',
    position: 'relative',
    textAlign: 'center',
    textShadow: '1px 1px 1px #000',
    marginTop:'10px',
    zIndex:1,
}

const captionBoxStyle = {
    bottom: '50px',
    display:'flex',
    justifyContent: 'center',
    position: 'absolute',
    textAlign: 'center',
    width:'100%',
}

const captionText = {
    background: 'rgba(0,0,0,0.4)',
    padding: '10px',
    color: '#fff',
}

export default UserDisplay