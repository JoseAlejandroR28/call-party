import  React, { Component } from 'react'  
import UserDisplay from './userdisplay'
import { socket } from './header'
import UserActions from './useractions'
import UserListItem from './userlistitem'
import ChatRoom from './chatroom'

import SpeechToText from 'speech-to-text'

if (typeof window !== "undefined") {
    var answersFrom = {}, offer;
        var peerConnection = window.RTCPeerConnection ||
            window.mozRTCPeerConnection ||
            window.webkitRTCPeerConnection ||
            window.msRTCPeerConnection;
    
        var sessionDescription = window.RTCSessionDescription ||
            window.mozRTCSessionDescription ||
            window.webkitRTCSessionDescription ||
            window.msRTCSessionDescription;
    
            navigator.getUserMedia  = navigator.getUserMedia ||
            navigator.webkitGetUserMedia ||
            navigator.mozGetUserMedia ||
            navigator.msGetUserMedia;
    
        var rtcChannelEmiter = new peerConnection({
            iceServers: [{
                url: "stun:stun.services.mozilla.com",
                username: "somename",
                credential: "somecredentials"
            }]
        });

        var rtcChannelReceiver = new peerConnection({
            iceServers: [{
                url: "stun:stun.services.mozilla.com",
                username: "somename",
                credential: "somecredentials"
            }]
        });

        socket.on('remove-user', function (id) {
            var div = document.getElementById(id);
            //document.getElementById('users').removeChild(div);
        });
    

       
}


function error(err) {
    console.warn('Error', err);
}

class Viewport extends Component {

    constructor(props) {
        super(props)
        this.state = {
            users : [],
            user: null,
            friend: null,
            srcFriend:null,
            srcLocal:null,
            isCallActive: false,
            isBroadcastActive:false,
            flag: true,
            listening: true,
            finalisedText: '',
            textCaption: '',
        }
    }

    addUser(user){
        let users = this.state.users
        users.push(user)
        this.setState({users})
    }

    componentDidMount() {
        const that = this

        socket.on('user-list', data => {

            let users = that.state.users

            users = data
            that.setState({users})
        })

        socket.on('add-users', data => {
            let users = that.state.users
            const user = {
                id: data.id,
                name: data.name,
            }
            users.push(user)

            that.setState({users})
            console.log(that.state.users)
        })


        rtcChannelEmiter.onaddstream = function (obj) {
          
            console.log(obj)
            that.setState({srcFriend: obj.stream})
        }

        socket.on('offer-made', function (data) {


            offer = data.offer;
            that.setState({
                friend: {
                    name: data.username,
                    id: data.socket
                }
            })
            console.log(that.state.friend)

            that.createAnswer(data.socket,
                that.props.user.id == data.socket? rtcChannelReceiver: rtcChannelEmiter,
                data)
    
        });
    
        socket.on('answer-made', function (data) {
           
            let pc = rtcChannelEmiter
            if (that.props.user.id == data.socket) {
                pc = rtcChannelReceiver
            }

            pc.setRemoteDescription(new sessionDescription(data.answer), function () {
                if (!that.state.isBroadcastActive) {

                    that.createOffer(data.socket, pc);
                    that.setState({isBroadcastActive:true})
                } 
            }, error)
        });

        socket.on('call-finished', function (data) {
            rtcChannelReceiver.removeStream(that.state.srcFriend)
            that.setState({srcFriend: null, textCaption:''})
            socket.emit('end-call',{to:that.state.friend.id})
        });

        socket.on('receive-subtitles', function (data) {
            console.log(data)
            that.setState({textCaption: data.text})
        });

        const onAnythingSaid = text => {
            this.setState({ interimText: text });
          };
      
          const onEndEvent = () => {
            if (this.state.listening) {
              this.listener.startListening();
            }
          };
      
          const onFinalised = text => {
            this.setState({
              finalisedText: [text, ...this.state.finalisedText],
              interimText: ''
            });
            console.log(that.state.friend)
            console.log(that.state.friend)
            if (that.state.isCallActive ) {
                console.log('text: '+text)
                const user = that.state.users.filter(u => u.id != that.props.user.id)[0]
                socket.emit('share-subtitles',{
                    text: text,
                    to: user.id,
                    username: user.name
                })
            }
          };
      
          try {
            this.listener = new SpeechToText(onFinalised, onEndEvent, onAnythingSaid);
            this.listener.startListening()
   
          } catch (error) {
            console.log(error)
            this.setState({ error: error.message });
          }
    }

    createAnswer = (id, pc, data) => {
        pc.setRemoteDescription(new sessionDescription(data.offer), function () {
            pc.createAnswer(function (answer) {
                pc.setLocalDescription(new sessionDescription(answer), function () {
                    socket.emit('make-answer', {
                        answer: answer,
                        to: data.socket
                    });
                }, error);
            }, error);
        }, error);
    }

    createOffer = (id, pc) => {
        
        pc.createOffer(function (offer) {
            pc.setLocalDescription(new sessionDescription(offer), function () {
                socket.emit('make-offer', {
                    offer: offer,
                    to: id
                });
            }, error);
        }, error);
    }

    handlerClick = () =>{
        const that = this
        const user = that.state.users.filter(u => u.id != that.props.user.id)
        console.log(user[0])

        if (this.state.isCallActive == false ) { 
            if (typeof navigator !== "undefined"){
                
                const qvgaConstraints = {
                    video: {width: {exact: 360}, height: {exact: 240}},
                    audio: true,
                };
                
                navigator.getUserMedia(qvgaConstraints, function (stream) {
                    
                    that.setState({srcLocal: stream})
                    rtcChannelEmiter.addStream(stream);
                    
                    
                    that.handlerCallUser(user[0])
                }, (error) => console.log(error))
            }
        } else {
            rtcChannelEmiter.removeStream(this.state.srcLocal)
            this.setState({srcLocal: null})
            socket.emit('call-end',{to:user[0].id})
        }
        
        this.setState({isCallActive:!this.state.isCallActive})
    }
    
    handlerCallUser = (user) => {
        this.createOffer(user.id, rtcChannelEmiter)
    }

    render(){

        const that = this
        

        return <div style={viewportStyle}>
                <div style={{}}>
                    <div style={{width:'50%', display:'inline-block', verticalAlign:'bottom'}}>
                        <UserDisplay user={this.props.user} isCallActive={this.state.isCallActive} stream={this.state.srcLocal} muted={true }></UserDisplay>
                    </div>
                    <div style={{width:'50%', display:'inline-block', verticalAlign:'bottom'}}>
                    {
                        this.state.users.filter(u => u.id != this.props.user.id).map((user,i) => {
                            return <UserDisplay key={i} user={user} stream={this.state.srcFriend} captions={this.state.textCaption} muted={false}></UserDisplay>
                        })
                    }
                    </div>
                </div>
                <div style={{marginTop:'30px'}}>
                    <ChatRoom user={this.props.user} />
                </div>
                
                {
                    (this.state.users.length > 1)? <UserActions handlerClick={this.handlerClick} isCallActive={this.state.isCallActive} />:''
                }
        </div>
    }
}
const viewportVideo = {
    width: '30%',
}
const viewportStyle = {
    background: '#333',
    borderTop: '1px solid #000',
    padding: '40px 10px',
    display:'inline-block',
    width: 'calc( 100% - 20px)',
}

const userListStyle = {
    listStyle:'none',
    margin:'0p',
    padding: '5px',
    width:'18%',
    display: 'inline-block',
}


export default Viewport