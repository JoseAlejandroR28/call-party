import  React, { Component } from 'react'  
import { socket } from './header'

class ChatRooom extends Component {

    constructor(props){
        super(props)
        this.state = {
            text: '',
            messages: [],
        }
    }

    componentDidMount(){
        const that = this
        socket.on('chat-public-receive', (data) => {
            let messages = that.state.messages

            messages.push(data)

            that.setState({messages})
        });
    }

    updateInputValue = (evt) => {
        this.setState({
          text: evt.target.value
        });
    }

    submit = (event) => {
        if (event.key == 'Enter') {
            socket.emit('chat-public-send',{username:this.props.user.name,text:this.state.text})
            this.setState({text:''})
        }
    }

    render(){
        return <div style={chatStyle}>  
            <div style={wallStyle}>
                {
                    this.state.messages.map((msg, id) => {
                        return <p key={id}><b>{msg.username}: </b> {msg.text}</p>
                    })
                }
            </div>
            <div style={boxInputStyle}>

                <input style={inputStyle} type="text"
                    value={this.state.text}
                    onChange={evt => this.updateInputValue(evt)}
                    onKeyDown={evt => this.submit(evt)}
                    placeholder={'message..'}
                 />
            </div>
        </div>
    }
}

const chatStyle = {
    with: '100%',
    height:'inherit',
} 

const wallStyle = {
    background:'#fff',
    height: '135px',
    padding:'10px',
    overflowY: 'scroll',
}

const boxInputStyle = {
    background:'#eee',
    border:'1px solid #bbb',
    padding: '10px',
    width: 'calc(100% - 22px)',
}
const inputStyle = {
    padding: '10px',
    width: 'calc(100% - 24px)',
    height:'20px',
}

export default ChatRooom