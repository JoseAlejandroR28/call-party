import React from 'react'

import { Header, socket } from './components/header'
import Viewport from './components/viewport'
import Footer from './components/footer'


class App extends React.Component {

    constructor() {
        super()

        this.state = {
            user: {
                id: null,
                name: null,
            },
            users: [],
        }
    }

    componentDidMount(){

        if (!('webkitSpeechRecognition' in window)) {
            throw new Error("This browser doesn't support speech recognition. Try Google Chrome.");
          }
          
        const that = this
        let nickname = null

        if (typeof window !== "undefined") {
            nickname = window.prompt("Username:")
        }

        socket.emit('register',{nickname: nickname})

        socket.on('access', (data) => {
            let user = that.state.user
            user.id = data.id
            user.name = data.nickname
            
            let users = that.state.users

            that.setState({user})
        })
          
    }

    render() {
        

        return <div style={appStyle}>
            <Header />
            <Viewport user={this.state.user} users={this.state.users} />
            <Footer />
        </div>
    }
}

const appStyle = {
    margin:'0px',
    height: '100%',
    padding: '0px',
    width:'100%',
}


export default App 