import ExpressServer from './application/ExpressServer'
import IOServer from './application/IOServer'
import IServerWebsocket from './business/server/IServerWebsocket'
import IServerHttp from './business/server/IServerHttp'


//const PORT    = process.env.PORT || 3000;
const next = require('next')
    
const dev = process.env.NODE_ENV !== 'production'
const app = next({ dev })
const handle = app.getRequestHandler()

const PORT    = 3000;
let https = require('https');
const fs = require('fs');

const options = {
  key: fs.readFileSync('./.cert/key.pem'),
  cert: fs.readFileSync('./.cert/cert.pem')
};

app.prepare()
.then(() => {
  const server:IServerHttp = new ExpressServer(PORT);
  const express = server.getApp();

  express.get('*', (req, res) => {
    return handle(req, res)
  })

  https = https.createServer(options, express).listen(PORT)

  const io:IServerWebsocket = new IOServer(https)
})
.catch((ex) => {
  console.error(ex.stack)
  process.exit(1)
})



/*const app = express();

app.set("port", process.env.PORT || 3000);

let http = require("http").Server(app);

app.use(express.static('public'));
app.use(express.json())
app.use(express.urlencoded({ extended: true }))
app.set('views', './views');
app.set('view engine', 'pug');
express.get('/', function(req, res) {
  res.sendFile('./index.html');
});
*/


//let io = require("socket.io")(http);
//Whenever someone connects this gets executed
/*io.on('connection', function(socket) {
   console.log('A user connected')

   //Whenever someone disconnects this piece of code executed
   socket.on('disconnect', function () {
      console.log('A user disconnected')
   })
})*/

/*const server = https.listen(PORT, function() {
  console.log("listening on *:3000")
});*/