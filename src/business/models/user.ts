import IOServer from '../../application/IOServer'
import ISocket from '../server/ISocket'

class User {

    private id: string

    private name:string

    private socket:ISocket

    constructor(id:string, socket: ISocket)
    {
        this.id = id
        this.socket = socket
    }

    public getId():string
    {
        return this.id
    }

    public getName():string
    {
        return this.name
    }

    public setName(name:string)
    {
        this.name = name
    }

    public getSocket():ISocket
    {
        return this.socket
    }
}

export default User