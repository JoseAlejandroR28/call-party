interface ISocket {

    emit(listener:string, ...args: any[])
    on(listener:string, ...args: any[])
}

export default ISocket