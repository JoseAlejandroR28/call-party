interface IServerWebsocket {

    to(name:string):IServerWebsocket

    send(...args: readonly any[]):IServerWebsocket

}

export default IServerWebsocket