
import * as express from "express";
import * as socketio from "socket.io";
import { createServer, Server } from 'https';
import IServerWebsocket from './../business/server/IServerWebsocket'
import ISocket from './../business/server/ISocket'
import User from './../business/models/user'


class IOServer implements IServerWebsocket, ISocket {

    protected server: socketio.Server;

    private socketsArray = []

    private usersData:User[]

    constructor(server: Server)
    {
        this.server = require("socket.io")(server);

        this.config()
    }

    private config() 
    {
        this.usersData = []

        setInterval(() => {
            let arr = this.usersData.filter(u => u.getName()!= undefined).map((u) => {
                return {id:u.getId(), name: u.getName()}
            })
            console.log(arr)
            this.server.sockets.emit('user-list',arr)
        },1000 * 10)

        this.server.on('connection', (socket) => {

            let user = new User(socket.id, socket)
            this.usersData.push(user)


            socket.emit('test', {
                a:1,b:2,
            });

            socket.on('register', data => {
                socket.emit('access', {id: socket.id, nickname: data.nickname})

                user.setName(data.nickname)

                socket.broadcast.emit('add-users', {
                    id: user.getId(),
                    name: user.getName(),
                });
            })

            socket.on('disconnect', () => {
                this.socketsArray.splice(this.socketsArray.indexOf(socket.id), 1);
                this.server.emit('remove-user', socket.id);
                this.usersData = this.usersData.filter(u => u.getId() != socket.id)
            });

            socket.on('chat-public-send', (data) => {
                this.server.emit('chat-public-receive', data);
            });

            socket.on('call-end', (data) => {
                socket.to(data.to).emit('call-finished', {
                    socket: socket.id,
                    username: user.getName(),
                });
            });

            socket.on('make-offer', (data) => {
                socket.to(data.to).emit('offer-made', {
                    offer: data.offer,
                    socket: socket.id,
                    username: user.getName(),
                });
            });

            socket.on('make-answer', (data) => {
                socket.to(data.to).emit('answer-made', {
                    socket: socket.id,
                    answer: data.answer,
                    username: user.getName(),
                });
            });

            socket.on('make-request', (data) => {
                console.log('make-request',data)
                socket.to(data.to).emit('request-made', {
                    socket: socket.id,
                    username: user.getName(),
                });
            });

            socket.on('share-subtitles', (data) => {
                socket.to(data.to).emit('receive-subtitles', {
                    socket: socket.id,
                    username: user.getName(),
                    text: data.text,
                });
            })

        });
    }

    public getServer(): socketio.Server {
        return this.server;
    }

    to(name: string): IOServer {
        this.server.to(name)
        return this
    }
    
    send(...args: readonly any[]): this {
        this.server.send(name)
        return this
    }

    emit(listener: string, ...args: any[]) {
        this.server.emit(listener, ...args)
    }


    on(listener: string, ...args: any[]) {

    }

}

export default IOServer