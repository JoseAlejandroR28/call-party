import * as express from "express";
import IServerHttp from './../business/server/IServerHttp'
import StaticPageRouter from './routing/StaticPagesRouter'

class ExpressServer implements IServerHttp {

    protected app: any;

    private port:string | number;

    constructor(port: string | number)
    {
        this.port = port

        this.app = express()
        
        this.config()

        this.router()
    }

    private config()
    {
        this.app.set("port", this.port);

        console.log(__dirname + '/../../public')
        this.app.use(express.static(__dirname + '/../../public'));

        //this.app.use('*/css',express.static('public/css'));
        //this.app.use('*/js',express.static('public/js'));
        //this.app.use('*/images',express.static('public/images'));

        this.app.use(express.json())
        
        this.app.use(express.urlencoded({ extended: true }))

        this.app.set('views', './src/views');

        this.app.set('view engine', 'pug');

    }

    private router()
    {
        const staticPageRouter = new StaticPageRouter(this.app)
    }

    public start()
    {
        this.app.listen(this.port, () => {
            console.log('Running server on port %s', this.port);
        });
    }

    public getApp(): any {
        return this.app;
    }
}

export default ExpressServer